﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra;
using Cassandra.Mapping;
using Cassandra.Data.Linq;



namespace cassandra_receiver.Cassandra
{
	
	public class Operation
    {

		static Cluster cluster;
		public static ISession SharedSession { get; set; } 
		public static Table<Event> EventTable { get; set; } 

		// this function creates keyspace and event table if not allready exists
		public static void SetupEnvironMent() {
			ISession session = cluster.Connect();
			session.Execute(Program.ReadConfigVariable("cassandra_local","keyspace"));
			session.Execute("use gyldendal;");
			var createDb = @"
CREATE TABLE IF NOT EXISTS gyldendal.event (
    datehour text,
    type text,
    contenttype text,
    data text,
    id text,
    source text,
    specversion text,
    time timestamp,
    PRIMARY KEY ((datehour, type), id))";
			session.Execute(createDb);
			var eventTable = new Table<Event>(session);
			SharedSession = session;
			EventTable = eventTable;
		}

		public static void SaveEvent(Event e) {
			EventTable.Insert(e).Execute();
		}
        public static void Connect()
        {
            Console.WriteLine("Connecting to Cassandra");
			cluster = Cluster.Builder().AddContactPoints("localhost").Build();
        
			Console.WriteLine("Got a connection");
        }     
    }
}
