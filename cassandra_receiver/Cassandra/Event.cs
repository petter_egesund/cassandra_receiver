﻿using System;
using Cassandra;
using Cassandra.Mapping.Attributes;
using Gyldendal.Insights.CloudEvent.Domain;
using Newtonsoft.Json;

namespace cassandra_receiver.Cassandra
{
    [Table]
    public class Event
    {
        [ClusteringKey]
		public string Id { get; set; }
        public string Data { get; set; }
        public string ContentType { get; set; }
        [ClusteringKey]
        public string Type { get; set; }
        public string Source { get; set; }
        public DateTimeOffset Time { get; set; }
        public string SpecVersion { get; set; }
        
        [PartitionKey]
        public string DateHour { get; set; }



        public static Event InsightMessageToEvent(InsightsMessage msg) {
            var e = new Event();
            e.ContentType = msg.ContentType;
            e.Data = JsonConvert.SerializeObject(msg.Data);
            e.Id = msg.Id.ToString();
            e.Source = msg.Source;
            e.Type = msg.Type;
            e.SpecVersion = msg.SpecVersion;
            e.DateHour = msg.Time.ToString("yyyy-MM-dd-hh");
            e.Time = msg.Time;
            return e;
        }
    }
}
