﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading;
using Serilog;


namespace cassandra_receiver
{
	public class Program
	{
       
		public static IConfiguration configuration { get; private set; }
		public static Serilog.ILogger RotationLogger { get; private set; }

		public static String ReadConfigVariable(String section, String name) {
			return configuration.GetSection(section).GetValue<String>(name);
		} 
      
		public static void Main(string[] args)
		{
            // read the config once
			configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("Properties/application.json")
				            .AddJsonFile("Properties/local.application.json")
				            .AddEnvironmentVariables()
                            .Build();


			RotationLogger = new LoggerConfiguration()
    			.WriteTo.RollingFile("log-{Date}.txt", retainedFileCountLimit: 5)
    			.CreateLogger();

			Cassandra.Operation.Connect();
			Cassandra.Operation.SetupEnvironMent();
			TestSBusManually();
			Console.WriteLine("The end");
		}

		public static void TestSBusManually() {
			var receiver = new SBus.Receiver();
            receiver.Start();
            Thread.Sleep(10000);
            receiver.Stop().GetAwaiter().GetResult(); 
		}
	}
}
