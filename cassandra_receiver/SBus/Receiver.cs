﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Gyldendal.Insights.CloudEvent.Domain;
using cassandra_receiver.Cassandra;
using Newtonsoft.Json;

namespace cassandra_receiver.SBus
{
    public class Receiver
    {
		ISubscriptionClient subscriptionClient;

        public Receiver()
        {
			subscriptionClient = SBus.Connector.GetTopicClientReader();
        }

		async Task ProcessMessagesAsync(Message message, CancellationToken token)
        {
            // Process the message.
            var msgBody = Encoding.UTF8.GetString(message.Body);
            // log all: Console.WriteLine($"Received message: SequenceNumber:{message.SystemProperties.SequenceNumber} Body:{msgBody}");
            Program.RotationLogger.Information("Writing msg: " + msgBody);

            InsightsMessage msg = JsonConvert.DeserializeObject<InsightsMessage>(msgBody);
            var e = Event.InsightMessageToEvent(msg);
            Cassandra.Operation.SaveEvent(e);
            
            // Complete the message so that it is not received again.
            // This can be done only if the subscriptionClient is created in ReceiveMode.PeekLock mode (which is the default).
            await subscriptionClient.CompleteAsync(message.SystemProperties.LockToken);
        }

		Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
		{
			Console.WriteLine($"Message handler encountered an exception {exceptionReceivedEventArgs.Exception}.");
			var context = exceptionReceivedEventArgs.ExceptionReceivedContext;
			Console.WriteLine("Exception context for troubleshooting:");
			Console.WriteLine($"- Endpoint: {context.Endpoint}");
			Console.WriteLine($"- Entity Path: {context.EntityPath}");
			Console.WriteLine($"- Executing Action: {context.Action}");
			return Task.CompletedTask;
		}

		public async Task Stop() {
			await subscriptionClient.CloseAsync();
		}

		public void Start() {
			var messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
            {
                // Maximum number of concurrent calls to the callback ProcessMessagesAsync(), set to 1 for simplicity.
                // Set it according to how many messages the application wants to process in parallel.
                MaxConcurrentCalls = 4,

                // Indicates whether MessagePump should automatically complete the messages after returning from User Callback.
                // False below indicates the Complete will be handled by the User Callback as in `ProcessMessagesAsync` below.
                AutoComplete = false
            };
			subscriptionClient.RegisterMessageHandler(ProcessMessagesAsync, messageHandlerOptions);
			Console.WriteLine("Started receiver");
		}
    }
}

