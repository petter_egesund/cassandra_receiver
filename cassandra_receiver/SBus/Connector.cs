﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;

namespace cassandra_receiver.SBus
{
    public class Connector
    {
		public static ISubscriptionClient GetTopicClientReader() {
			var serviceBusConnectionString = Program.ReadConfigVariable("sbus_local", "cstring");
			var topicName = Program.ReadConfigVariable("sbus", "topic");
			var subscription = Program.ReadConfigVariable("sbus", "subscription");
			var topicClient = new SubscriptionClient(serviceBusConnectionString, topicName, subscription);
			return topicClient;
         }

		public static TopicClient GetTopicClientWriter()
        {
			var serviceBusConnectionString = Program.ReadConfigVariable("sbus_local", "cstring");
			var topicName = Program.ReadConfigVariable("sbus", "topic");
			var topicClient = new TopicClient(serviceBusConnectionString, topicName);
            return topicClient;
        }
    }
}
